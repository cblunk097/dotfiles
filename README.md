# dotfiles

collection of dotfiles for my minimalist artix box

## [kanagawa with river](https://gitlab.com/cblunk097/dotfiles/-/tree/kanagawa-river?ref_type=heads)

![kanagawa with river](images/kanagawa-river.png)

## [tokyonight with hyprland](https://gitlab.com/cblunk097/dotfiles/-/tree/tokyonight-hyprland?ref_type=heads)

![tokyonight with hyprland](images/tokyonight-hyprland.png)
